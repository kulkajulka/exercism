import java.util.Arrays;

public class Matrix {
    String matrix;
    public Matrix(String matrixAsString) {
        matrix = matrixAsString;
    }

    public int getRowsCount() {
        return matrix.split("[\n]").length;
    }

    public int getColumnsCount() {
        return matrix.split("[\n]")[0].split("[ ]").length;
    }

    public int[] getRow(int i) {
        return Arrays.stream(matrix.split("[\n]")[i].split("[ ]")).mapToInt(Integer::parseInt).toArray();
    }

    public int[] getColumn(int i) {
        int[] col = new int[matrix.split("[\n]").length];
        String[] rows = matrix.split("[\n]");
        for (int j = 0; j < col.length; j++) {
            col[j] = Integer.parseInt(rows[j].split("[ ]")[i]);
        }
        return col;
    }
}
