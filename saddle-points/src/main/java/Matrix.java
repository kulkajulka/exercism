import jdk.nashorn.internal.runtime.arrays.ArrayIndex;

import java.net.CookieHandler;
import java.util.*;

public class Matrix {
    private int[][] matrix;
    private int nrows;
    private int ncols;
    public Matrix(List<List<Integer>> lists) {
        if (lists.isEmpty()) {
            matrix = new int[0][0];
            nrows = 0;
            ncols = 0;
            return;
        }
        nrows = lists.size();
        ncols = lists.get(0).size();
        matrix = new int[nrows][ncols];
        for (int i = 0; i < nrows; i++) {
            matrix[i] = lists.get(i).stream().mapToInt(a -> a).toArray();
        }
    }

    public Set<MatrixCoordinate> getSaddlePoints() {
        if (matrix.length == 0) return Collections.emptySet();
        Set<MatrixCoordinate> finalSet = new HashSet<>();
        for (int i = 0; i < nrows; i++) {
            for (int j = 0; j < ncols; j++) {
                /*if (isHighestInCol(matrix[i][j],j)){
                    if (isLowestInRow(matrix[i][j],i)) finalSet.add(new MatrixCoordinate(i,j));
                }*/
                if (isLowestInCol(matrix[i][j],j)){
                    if (isHighestInRow(matrix[i][j],i)) finalSet.add(new MatrixCoordinate(i,j));
                }
            }
        }
        return finalSet;
    }

    public String toString(){
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < nrows; i++) {
            for (int j = 0; j < ncols; j++) {
                builder.append(matrix[i][j]).append(" ");
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        Matrix matrix = new Matrix(Arrays.asList(
                    Arrays.asList(4, 5, 4),
                    Arrays.asList(3, 5, 5),
                    Arrays.asList(1, 5, 4)
        ));
        Set<MatrixCoordinate> wynik = matrix.getSaddlePoints();
        Iterator<MatrixCoordinate> iterator = wynik.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

    private boolean isLowestInRow(int point,int row){
        boolean result = true;
        for (int i = 0; i < ncols; i++) {
            if (matrix[row][i] < point) result = false;
        }
        return result;
    }

    private boolean isHighestInRow(int point,int row){
        boolean result = true;
        for (int i = 0; i < ncols; i++) {
            if (matrix[row][i] > point) result = false;
        }
        return result;
    }

    private boolean isLowestInCol(int point,int col){
        boolean result = true;
        for (int i = 0; i < nrows; i++) {
            if (matrix[i][col] < point) result = false;
        }
        return result;
    }

    private boolean isHighestInCol(int point,int col){
        boolean result = true;
        for (int i = 0; i < nrows; i++) {
            if (matrix[i][col] > point) result = false;
        }
        return result;
    }
}
