import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class HandshakeCalculator {

    List<Signal> calculateHandshake(int number) {
        ArrayList<Signal> list = new ArrayList<>();
        String numberS = Integer.toBinaryString(number);
        if (numberS.charAt(numberS.length()-1) == '1') list.add(Signal.WINK);
        if(numberS.length() > 1) {
            if (numberS.charAt(numberS.length()-2) == '1') list.add(Signal.DOUBLE_BLINK);
        }
        if(numberS.length() > 2) {
            if (numberS.charAt(numberS.length()-3) == '1') list.add(Signal.CLOSE_YOUR_EYES);
        }
        if(numberS.length() > 3) {
            if (numberS.charAt(numberS.length()-4) == '1') list.add(Signal.JUMP);
        }
        if(numberS.length() > 4) {
            if (numberS.charAt(numberS.length()-5) == '1') Collections.reverse(list);
        }
        return list;
    }

}
