package com.devskiller.reflection;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Devskiller
 */
public class MethodListHelperTest {

	@Test
	public void shouldReturnDeclaredMethods() throws Exception {
		// given
		MethodListHelper methodListHelper = new MethodListHelper(SampleRepository.class,false,false);

		// when
		Collection<MethodInfo> methodInfos = methodListHelper.getMethodInfos();

		// then
		assertThat(methodInfos).containsOnly(
				new MethodInfo("save", false, Collections.singletonList(Object.class), void.class),
				new MethodInfo("find", false, Collections.singletonList(Long.class), Object.class),
				new MethodInfo("findAll", false, Collections.emptyList(), List.class)
		);
	}

	@Test
	public void shouldReturnInheritedAbstractMethod() throws Exception {
		// given
		MethodListHelper methodListHelper = new MethodListHelper(SampleRepository.class,true,true);	

		// when
		Collection<MethodInfo> methodInfos = methodListHelper.getMethodInfos();

		// then
		assertThat(methodInfos).containsOnly(
				new MethodInfo("save", false, Collections.singletonList(Object.class), void.class),
				new MethodInfo("find", false, Collections.singletonList(Long.class), Object.class),
				new MethodInfo("findAll", false, Collections.emptyList(), List.class),
				new MethodInfo("countAll", true, Collections.emptyList(), Long.class)
		);
	}
}