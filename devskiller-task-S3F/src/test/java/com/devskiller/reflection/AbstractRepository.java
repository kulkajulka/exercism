package com.devskiller.reflection;

/**
 * @author Devskiller
 */
abstract class AbstractRepository {
	abstract Long countAll();
}
