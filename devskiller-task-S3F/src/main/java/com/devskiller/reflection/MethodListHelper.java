package com.devskiller.reflection;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;

public class MethodListHelper {

	private final Class<?> _class;
	private final Set<MethodInfo> methodInfosSet;
	private final boolean includeAbstract;
	private final boolean includeSuperclass;

	/**
	 * Constructor
	 * 
	 * @param aClass
	 *            class to analyse
	 * @param includeAbstract
	 *            should we include abstract methods
	 * @param includeSuperclass
	 *            should we include methods from the class we extend (only direct
	 *            superclass)
	 */

	public MethodListHelper(Class<?> aClass, boolean includeAbstract, boolean includeSuperclass) {
		this._class = aClass;
		this.includeAbstract = includeAbstract;
		this.includeSuperclass = includeSuperclass;
		this.methodInfosSet = methodsListToMethodsInfoHashSet();
	}

	public Collection<MethodInfo> getMethodInfos() {
		return methodInfosSet;
	}

	private Set<MethodInfo> methodsListToMethodsInfoHashSet() {
		Set<MethodInfo> set = new HashSet<>();
		List<Method> list = createMethodsList();
		for (Method method : list) {
			set.add(methodToMethodInfo(method));
		}
		return set;
	}

	private MethodInfo methodToMethodInfo(Method method) {
		String name = method.getName();
		List<Class<?>> args = Arrays.asList(method.getParameterTypes());
		Class<?> returnType = method.getReturnType();
		Boolean isAbstract = Modifier.isAbstract(method.getModifiers());
		MethodInfo methodInfo = new MethodInfo(name, isAbstract, args, returnType);
		return methodInfo;
	}

	private List<Method> createMethodsList() {
		List<Method> list = new ArrayList<>(Arrays.asList(_class.getDeclaredMethods()));
		if (includeSuperclass) {
			includeSuperclassMethods(list);
		}
		if (!includeAbstract) {
			removeAbstractMethods(list);
		}
		return list;
	}

	private void includeSuperclassMethods(List<Method> list) {
		list.addAll(Arrays.asList(_class.getSuperclass().getDeclaredMethods()));
	}

	private void removeAbstractMethods(List<Method> list) {
		list.removeIf(m -> Modifier.isAbstract(m.getModifiers()));
	}

}
