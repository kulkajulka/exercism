This is DevSkiller task I was asked to implement during recruitment process for intern position in one of the leading IT companies.

# Method reflections

This project uses **Java Reflection API** to describe methods declared in classes.

Your job is to implement `MethodListHelper.listMethods` method. There are several unit tests in `MethodListHelperTest` that will help you to verify your solution.
