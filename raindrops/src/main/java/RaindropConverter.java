class RaindropConverter {
    String word = "";
    String convert(int number) {
        if (number%3==0) word+="Pling";
        if (number%5==0) word+="Plang";
        if (number%7==0) word+="Plong";
        if (number%3!= 0 && number%5!=0 && number%7!=0) return String.valueOf(number);
        return word;
    }

}
