import java.util.Arrays;
import java.util.stream.Stream;

class TwelveDays {
    String[] tableOfLyrics = new String[]{
            " a Partridge in a Pear Tree.\n",
            " two Turtle Doves,",
            " three French Hens,",
            " four Calling Birds,",
            " five Gold Rings,",
            " six Geese-a-Laying,",
            " seven Swans-a-Swimming,",
            " eight Maids-a-Milking,",
            " nine Ladies Dancing,",
            " ten Lords-a-Leaping,",
            " eleven Pipers Piping,",
            " twelve Drummers Drumming,",
    };
    String[] lyrics = new String[]{"first","second","third","fourth","fifth","sixth","seventh","eighth","ninth",
            "tenth","eleventh","twelfth"};
    String onThe = "On the ";
    String dayOf = " day of Christmas my true love gave to me,";
    String and = " and";
    String verse(int verseNumber) {
        if (verseNumber == 1) return onThe+lyrics[0]+dayOf+tableOfLyrics[0];
        StringBuilder returnString = new StringBuilder();
        returnString.append(onThe).append(lyrics[verseNumber-1]).append(dayOf);
        for(int i = verseNumber-1;i>0;i--){
            returnString.append(tableOfLyrics[i]);
        }
        return returnString.append(and).append(tableOfLyrics[0]).toString();
    }

    String verses(int startVerse, int endVerse) {
        StringBuilder returnS = new StringBuilder();
        for (int j= startVerse;j < endVerse ;j++){
            returnS.append(verse(j)).append("\n");
        }
        return returnS.append(verse(endVerse)).toString();
    }
    
    String sing() {
        return verses(1,12);
    }
}
