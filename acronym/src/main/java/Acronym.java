class Acronym {
    String phrase;
    String acronym;
    Acronym(String phrase) {
        this.phrase = phrase;
    }

    String get() {
        StringBuilder builder = new StringBuilder();
        String[] stringTable = phrase.split("[ -]");
        if(phrase.isEmpty() || phrase == null) return "";
        else {
            for (int i = 0; i < stringTable.length; i++) {
                builder.append(stringTable[i].charAt(0));
            }
            acronym = builder.toString().toUpperCase();
        }
        return acronym;
    }

}
