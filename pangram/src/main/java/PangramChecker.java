public class PangramChecker {

    public boolean isPangram(String input) {
        input = input.toLowerCase();
        for (int i = 'a';i<='z';i++){
            if (!input.contains(String.valueOf((char)i))) return false;
        }
        return true;
    }
}
