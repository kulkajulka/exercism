import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.google.common.collect.Lists;


class ReverseString {
  
    String reverse(String phrase) {
    	return new StringBuilder(phrase).reverse().toString();
    	//return Lists.reverse(Arrays.asList(phrase.split(""))).stream().collect(Collectors.joining());
    }
    
}