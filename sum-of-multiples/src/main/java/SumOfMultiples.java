import java.util.Arrays;
import java.util.stream.IntStream;

class SumOfMultiples {
    int number;
    int[] set;
    SumOfMultiples(int number, int[] set) {
        this.number = number;
        this.set = set;
    }

    int getSum() {
        return IntStream.range(1,number).filter(x -> {
            for (int i = 0; i < set.length; i++) {
                if(x%set[i] == 0) return true;
            }
            return false;
        }).sum();
    }

}
