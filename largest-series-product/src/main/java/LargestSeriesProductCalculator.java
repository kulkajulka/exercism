import io.vavr.collection.Stream;

import java.util.Arrays;

public class LargestSeriesProductCalculator {
    String series;

    public LargestSeriesProductCalculator(String string) {
        if (string == null)
            throw new IllegalArgumentException("String to search must be non-null.");
        else if (string.matches(".*\\D.*"))
            throw new IllegalArgumentException("String to search may only contains digits.");
        else series = string;
    }

    public long calculateLargestProductForSeriesLength(int i) {
        if (i < 0) throw new IllegalArgumentException("Series length must be non-negative.");
        else if (i > series.length())
            throw new IllegalArgumentException(
                    "Series length must be less than or equal to the length of the string to search.");
        if (series.isEmpty() || i < 1) return 1;
        long product = Arrays.stream(series.split(""))
                .mapToLong(Long::parseLong)
                .limit(i)
                .reduce(1,(a, b) -> a * b);
        int[] table = Arrays.stream(series.split(""))
                .mapToInt(Integer::parseInt).toArray();
        for (int j = 1; j < series.length()-i+1; j++) {
            long temp=1;
            for (int k = j; k < j+i; k++) {
                temp *= table[k];
            }
            if(product < temp)
                product = temp;
        }
        return product;
        /*return Stream.of(series.split("")).map(Long::parseLong).sliding(i)
                .map(w -> w.fold(1L, (a,b) -> a*b)*//*w->w.product().longValue()*//*)
                .max().get();*/
    }
}

