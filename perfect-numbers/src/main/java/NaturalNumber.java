import java.util.stream.IntStream;

class NaturalNumber {
    int number;
    NaturalNumber(int n){
        if (n <1) throw new IllegalArgumentException("You must supply a natural number (positive integer)");
        else number = n;
    }
    Classification getClassification(){
        int sum = IntStream.range(1,number).filter(x -> number%x == 0).sum();
        if (sum == number) return Classification.PERFECT;
        if (sum < number) return Classification.DEFICIENT;
        else return Classification.ABUNDANT;
    }

}
