public class CollatzCalculator {
    int count = 0;
    public int computeStepCount(int i) {
        if (i < 1) throw new IllegalArgumentException("Only natural numbers are allowed");
        if (i == 1) return count;
        count++;
        return i%2 == 0 ? computeStepCount(i/2) : computeStepCount(i*3+1);
    }
}
