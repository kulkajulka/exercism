import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class KindergartenGarden {
    String plants;
    String[] students;
    public KindergartenGarden(String plants) {
        this.plants = plants;
        students = new String[]{"Alice", "Bob", "Charlie", "David",
                "Eve", "Fred", "Ginny", "Harriet",
                "Ileana", "Joseph", "Kincaid", "Larry"};
    }
    public KindergartenGarden(String plants, String[] studentsArray){
        this.plants =plants;
        students = Arrays.stream(studentsArray).sorted().toArray(String[]::new);
    }

    public List<Plant> getPlantsOfStudent(String student) {
        int i = 0;
        for (i = 0; i < students.length; i++) {
            if (student.equals(students[i])) break;
        }
        List<Plant> plantsList = new ArrayList<>();
        plantsList.add(Plant.getPlant(plants.charAt(2*i)));
        plantsList.add(Plant.getPlant(plants.charAt(2*i+1)));
        plantsList.add(Plant.getPlant(plants.charAt(plants.indexOf("\n")+1+i*2)));
        plantsList.add(Plant.getPlant(plants.charAt(plants.indexOf("\n")+1+i*2+1)));
        return plantsList;
    }
}
