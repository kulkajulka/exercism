import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RotationalCipher {
    private int rot;
    Pattern reGex = Pattern.compile("[a-zA-Z]");
    public RotationalCipher(int rot) {
        this.rot = rot;
    }
    /* This is horrible, but I couldn't figure out anything else :( */
    public String rotate(String a) {
        char[] charA = a.toCharArray();
        for (int i = 0; i < charA.length; i++) {
            if (charA[i] <= 'Z' && charA[i] >= 'A') {
                charA[i] = charA[i]+rot > 'Z' ? (char)(charA[i]+rot -26) : (char)(charA[i]+rot);
            }
            else if (charA[i] <= 'z' && charA[i] >= 'a'){
                charA[i] = charA[i]+rot > 'z' ? (char)(charA[i]+rot -26) : (char)(charA[i]+rot);
            }
            else {}
        }
       return String.valueOf(charA);
    }
}
