public class Hamming {
    String left;
    String right;
    int diff = 0;

    Hamming(String leftStrand, String rightStrand) {
        if(leftStrand.length() != rightStrand.length()){
            throw new IllegalArgumentException("leftStrand and rightStrand must be of equal length.");
        }
        else {
            left = leftStrand;
            right = rightStrand;
        }
    }

    int getHammingDistance() {
        for (int i = 0;i<left.length();i++){
            if (left.charAt(i) != right.charAt(i)) diff++;
        }
        return diff;
    }

}
