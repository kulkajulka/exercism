import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RnaTranscription {
    public String transcribe(String dnaStrand) {
        return Stream.of(dnaStrand.split("")).map(x->{
            if (x.equals("A")) return "U";
            else if (x.equals("C")) return "G";
            else if (x.equals("G")) return "C";
            else if (x.equals("T"))return "A";
            else return "";
                }
        ).collect(Collectors.joining());
    }
}