class Scrabble {
    String word = "";
    int score = 0;
    Scrabble(String word) {
        this.word = word;
    }

    int getScore() {
        for (int i = 0;i<word.length();i++){
            if(String.valueOf(word.charAt(i)).matches("[AEIOULNRSTaeioulnrst]")) score+=1;
            else if(word.substring(i,i+1).matches("[DGdg]")) score+=2;
            else if(word.substring(i,i+1).matches("[BCMPbcmp]")) score+=3;
            else if(word.substring(i,i+1).matches("[FHVWYfhvwy]")) score+=4;
            else if(word.substring(i,i+1).matches("[Kk]")) score+=5;
            else if(word.substring(i,i+1).matches("[JXjx]")) score+=8;
            else if(word.substring(i,i+1).matches("[QZqz]")) score+=10;
        }
        return score;
    }

}
