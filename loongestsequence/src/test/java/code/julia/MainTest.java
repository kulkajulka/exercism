package code.julia;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class MainTest {
    private Main newMain;

    @Before
    public void setup(){newMain = new Main();}

    @Test
    public void emptyString(){
        int length  = newMain.longestSequence("");
        assertEquals(0,length);
    }

    @Test
    public void spaceString(){
        int length = newMain.longestSequence(" ");
        assertEquals(1,length);
    }

    @Test
    public void numbers(){
        assertEquals(1,newMain.longestSequence("0123456789"));
    }

    @Test
    public void signsString(){
        assertEquals(2,newMain.longestSequence("%&(_)#S  (*#"));
    }

    @Test
    public void someString(){
        assertEquals(9,newMain.longestSequence("   555555ggggggggg"));
    }

    @Test
    public void nullString(){
        try {
            newMain.longestSequence(null);
            fail("Expected GivenNull.class");
        } catch (Exception e){
            assertEquals("Null is not a sequence of characters!",e.getMessage());
        }
    }

    @Test (expected = Main.GivenNull.class)
    public void nullStringException(){
        newMain.longestSequence(null);
    }


    @Test
    public void longSpaces(){
        assertEquals(20,newMain.longestSequence("                    "));
    }

    @Test
    public void slashes(){
        assertEquals(5,newMain.longestSequence("/////"));
    }

    @Test
    public void enters(){
        assertEquals(5,newMain.longestSequence("\n\n\n\n\n"));
    }

    @Test
    public void comasAndEars(){
        assertEquals(8,newMain.longestSequence("'''''''',,,,<,>,???"));
    }
    @Test
    public void tabulators(){
        assertEquals(4,newMain.longestSequence("\t\t\t\t"));
    }
    @Test
    public void strangeSings(){
        assertEquals(5,newMain.longestSequence("\u008C\u008C\u008C®®®®®"));
    }
    @Test
    public void stringNull(){
        assertEquals(2,newMain.longestSequence("null"));
    }
    @Test
    public void charSequenceAtBegin(){
        assertEquals(3,newMain.longestSequence("mm mmm 1234"));
    }
    @Test
    public void charSequenceAtEnd(){
        assertEquals(11,newMain.longestSequence("mmm mm1 11111111111"));
    }

    @Test
    @Parameters({"null,2",
            ",0",
            "omg,1",
            "\"\u008C\u008C\u008C®®®®®\",5",
            "\"\n\n\n\n\n\",5",
            "null,2"
    })
    @TestCaseName("Longest chain in \"{0}\" has length {1}")
    public void parametrized(String input, int expected){
        assertEquals(expected,newMain.longestSequence(input));
    }

}