package code.julia;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
@RunWith(JUnitParamsRunner.class)
public class PrimeNumberTest {
    private PrimeNumber primeNumber;
    @Before
    public void setup(){
        primeNumber = new PrimeNumber();
    }
    @Test
    public void zeroIsNotPrime(){
        assertFalse(primeNumber.isPrime(0));
    }

    @Test
    public void minusOneIsNotPrime(){
        assertFalse(primeNumber.isPrime(-1));
    }

    @Test
    public void maxInt(){
        assertTrue(primeNumber.isPrime(Integer.MAX_VALUE));
    }

    @Test
    public void minInt(){
        assertFalse(primeNumber.isPrime(Integer.MIN_VALUE));
    }

    @Test
    @Parameters({
            "2",
            "3",
            "5",
            "7",
            "15487319"
    })
    public void primeNumbers(int prime){ assertTrue(primeNumber.isPrime(prime));}

    @Test
    @Parameters({
            "1",
            "10",
            "14",
            "1992",
            "7000",
            "15487318"
    })
    public void notPrimeNumbers(int prime){ assertFalse(primeNumber.isPrime(prime));}



}