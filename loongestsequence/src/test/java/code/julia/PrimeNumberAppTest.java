package code.julia;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Scanner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(JUnitParamsRunner.class)
public class PrimeNumberAppTest {
    private Scanner scanner;
    private PrimeNumber primeNumber;
    private PrimeNumberApp app;

    @Before
    public void setup() {
        scanner = mock(Scanner.class);
        primeNumber = mock(PrimeNumber.class);
        app = new PrimeNumberApp(primeNumber);
    }

    @Test
    public void trueCheckTest() {
        when(primeNumber.isPrime(anyInt())).thenReturn(true);
        assertTrue(app.check());
    }

    @Test
    public void falseCheckTest() {
        when(primeNumber.isPrime(anyInt())).thenReturn(false);
        assertFalse(app.check());
    }

    @Test
    public void exceptionCheckTest() {
        when(primeNumber.isPrime(anyInt()))
                .thenThrow(new NumberFormatException("Wrong format!"));
        try {
            app.check();
            fail("Expected exception!");
        } catch (NumberFormatException e) {
            assertEquals("Wrong format!", e.getMessage());
        }
    }

    @Test
    @Parameters({
            "",
            "sdfbb",
            "pięć",
            "one",
            "kopytko"
    })
    public void wrongNumberFormatGiven(String string) {
        when(scanner.nextLine()).thenReturn(string);
        try {
            app.go(scanner);
        } catch (NumberFormatException e) {
            assertEquals("Wrong format!", e.getMessage());
            return;
        }
    }

    @Test
    @Parameters({
            " 3",
            "7   ",
            " 11 ",
            "15487319"
    })
    public void primeNumbersWithSpaces(String string){
        when(scanner.nextLine()).thenReturn(string);
        assertTrue(app.go(scanner));
    }

    @Test
    @Parameters({
            " 4",
            "72   ",
            " 110 ",
            "15487318"
    })
    public void notPrimeNumbersWithSpaces(String string){
        when(scanner.nextLine()).thenReturn(string);
        assertFalse(app.go(scanner));
    }
}