package code.julia;

import java.util.Scanner;

public class Main {

    public int longestSequence(String s) {
        if (s == null) throw new GivenNull();
        if(s.isEmpty()) return 0;
        int length = 1;
        int pointer = 0;
        int tempL = 1;
        while(pointer < s.length()-1){
            if (s.charAt(pointer) == s.charAt(pointer+1))
                tempL++;
            else
                tempL = 1;
            if (tempL > length) length = tempL;
            pointer++;
        }
        return length;
    }

    class GivenNull extends RuntimeException{
        private String message = "Null is not a sequence of characters!";

        @Override
        public String getMessage() {
            return message;
        }
    }

    public static void main(String[] args) {
        System.out.print("Witaj w programie! Podaj liczbe naturalną: ");
        Scanner scanner = new Scanner(System.in);
        PrimeNumber primeNumber = new PrimeNumber();
        PrimeNumberApp app = new PrimeNumberApp(primeNumber);
        while(scanner.hasNext()){
            try {
                app.print(app.go(scanner));
            } catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
    }
}
