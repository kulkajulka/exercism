package code.julia;

import java.util.Scanner;

public class PrimeNumberApp {
    private String number;
    private PrimeNumber primeNumber;
    public PrimeNumberApp(PrimeNumber primeNumber){
     this.primeNumber = primeNumber;
     number = "0";
    }

    void askForNumber(Scanner scanner){
        //System.out.print("Podaj liczbę naturalną: ");
        number = scanner.nextLine();
        if (number.isEmpty()) throw new NumberFormatException("Wrong format!");
        number = number.trim();
    }

    boolean check(){
        return primeNumber.isPrime(Integer.parseInt(number));
    }

    public boolean go(Scanner scanner){
        askForNumber(scanner);
        try {
            return check();
        } catch (NumberFormatException e){
            throw new NumberFormatException("Wrong format!");
        }
    }

    public static void print(boolean answer){
        if(answer){
            System.out.println("Liczba pierwsza!");
        } else {
            System.out.println("Liczba nie jest pierwsza!");
        }
    }
}
