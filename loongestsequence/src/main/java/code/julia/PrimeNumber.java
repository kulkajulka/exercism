package code.julia;

public class PrimeNumber {
    public PrimeNumber(){}
    public boolean isPrime(int number){
        if (number <= 1) return false;
        for(int i = 2; i < number/2 + 1; i++){
            if (number%i == 0) return false;
        }
        return true;
    }
}
