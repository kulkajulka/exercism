import java.util.stream.IntStream;

public class DifferenceOfSquaresCalculator {
    int sumOfSquares;
    int squareOfSum;
    int computeSquareOfSumTo(int n){
        return IntStream.range(1,n+1).sum()*IntStream.range(1,n+1).sum();
    }
    int computeSumOfSquaresTo(int n){
        return IntStream.range(1,n+1).map(x -> x*x).sum();
    }
    int computeDifferenceOfSquares(int n){
        return computeSquareOfSumTo(n) - computeSumOfSquaresTo(n);
    }
}
