import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

class LuhnValidator {
    boolean isValid(String candidate) {
        if (candidate.length() < 2) return false;
        if (!candidate.matches("[0-9 ]+")) return false;
        int[] num = Arrays.stream(candidate.split(""))
                //.filter(x -> !x.equals(" "))

                .filter(x -> x.matches("[0-9]"))
                .mapToInt(Integer::parseInt)
                .toArray();
        if (num.length<2) return false;
        for (int i = num.length-2; i>=0; i=i-2) {
            int pom = num[i]*2;
            if(pom > 9) pom = pom - 9;
            num[i] = pom;
        }
        return Arrays.stream(num).sum()%10 == 0;
    }
}
