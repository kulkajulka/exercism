
package com.julia;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GoldPrice {

    @SerializedName("data")
    @Expose
    private String data;
    @SerializedName("cena")
    @Expose
    private double cena;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        return new StringBuilder().append(getData()).append(" ").append(getCena())
                .toString();
    }

}
