package com.julia;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;

public interface NBPService {

    @GET("/api/cenyzlota/last/{last}?format=json")
    Call<List<GoldPrice>> query(
            @Path("last") int last
    );
}
