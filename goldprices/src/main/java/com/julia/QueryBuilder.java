package com.julia;

import jdk.nashorn.internal.codegen.CompilerConstants;
import retrofit2.Call;

import javax.management.Query;
import java.util.List;

public class QueryBuilder {
    private int last;
    private NBPService nbpService;

    public QueryBuilder(int last, NBPService nbpService) {
        this.nbpService = nbpService;
        validate(last);
        this.last = last;

    }

    public Call<List<GoldPrice>> build() {
        return nbpService.query(last);
    }

    private void validate(int last) {
        if (last < 1 || last > 93) throw new IllegalArgumentException("Tylko dodatnie i mniejsze od  wartości!");

    }
}
