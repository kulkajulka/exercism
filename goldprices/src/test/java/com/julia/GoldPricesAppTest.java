package com.julia;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(JUnitParamsRunner.class)
public class GoldPricesAppTest {
    QueryBuilder queryBuilder;

    @Test
    public void rekomendacjaKupujTest(){
        assertEquals(GoldPricesApp.Rekomendacja.KUPUJ,GoldPricesApp.rekomendacja(200,1));
    }

    @Test
    public void rekomendacjaTrzymajTest(){
        assertEquals(GoldPricesApp.Rekomendacja.TRZYMAJ,GoldPricesApp.rekomendacja(150,150));
    }

    @Test
    public void rekomendacjaSprzedawajTest(){
        assertEquals(GoldPricesApp.Rekomendacja.SPRZEDAWAJ,GoldPricesApp.rekomendacja(1,200));
    }

    @Test (expected = IllegalArgumentException.class)
    public void rekomendacjaZerotest(){
        GoldPricesApp.rekomendacja(0,0);
    }

    @Test (expected = IllegalArgumentException.class)
    public void rekomendacjaUjemnetest(){
        GoldPricesApp.rekomendacja(-5,-10);
    }

    @Test (expected = IllegalArgumentException.class)
    public void emptyList(){
        Writer writer = mock(Writer.class);
        GoldPricesApp.print(Collections.emptyList(),writer);
    }

    @Test
    public void oneElementList(){
        Writer writer = mock(Writer.class);
        List<GoldPrice> lista = mock(List.class);
        when(lista.size()).thenReturn(1);
        GoldPricesApp.print(lista,writer);
        verify(lista,times(2)).size();
    }

    @Test
    public void threeElementsList(){
        Writer writer = mock(Writer.class);
        List<GoldPrice> lista = mock(List.class);
        when(lista.size()).thenReturn(3);
        GoldPricesApp.print(lista,writer);
        verify(lista,times(2)).size();
    }

    @Test
    public void fourElementsList(){
        Writer writer = mock(Writer.class);
        List<GoldPrice> lista = mock(List.class);
        when(lista.size()).thenReturn(4);
        try {
            GoldPricesApp.print(lista,writer);
        } catch (IllegalArgumentException e){
            e.getMessage();
        }
        verify(lista,times(3)).size();
    }

    @Test
    public void executeSuccessfulResponse() throws IOException {
        Call call = mock(retrofit2.Call.class);
        Response<List> response = Response.success(Collections.singletonList(GoldPrice.class));
        when(call.execute()).thenReturn(response);
        assertTrue(GoldPricesApp.execute(call) instanceof List);
    }

    @Test
    public void executeUnsuccessfulResponse() throws IOException {
        Response<List> response = Response.error(400, ResponseBody.create(MediaType.parse("body"),"body"));
        retrofit2.Call call = mock(retrofit2.Call.class);
        when(call.execute()).thenReturn(response);
        assertEquals(Collections.emptyList(),GoldPricesApp.execute(call));
    }

    @Test (expected = IllegalArgumentException.class)
    @Parameters({
        "-1",
        "0",
        "",
        "-10",
        "100"
    })
    public void queryBuilderInvalidInputTest(int value){
        NBPService nbpService = mock(NBPService.class);
        queryBuilder = new QueryBuilder(value,nbpService);
    }


}