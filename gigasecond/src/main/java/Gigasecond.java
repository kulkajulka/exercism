import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

class Gigasecond {

    LocalDateTime data;
    Gigasecond(LocalDate birthDate) {
        data = LocalDateTime.of(birthDate,LocalTime.of(0,0,0,0));
    }

    Gigasecond(LocalDateTime birthDateTime) {
        data = birthDateTime;
    }

    LocalDateTime getDate() {
        return data.plusSeconds(1000000000);
    }

}
