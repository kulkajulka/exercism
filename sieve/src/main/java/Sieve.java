import java.util.ArrayList;
import java.util.List;

class Sieve {
    int maxPrime;
    List<Integer> listOfPrimes;
    Sieve(int maxPrime) {
        this.maxPrime = maxPrime;
        listOfPrimes = new ArrayList<>();
    }

    List<Integer> getPrimes() {
        listOfPrimes.add(2);
        int nr = 3;
        while (nr <= maxPrime){
            boolean isPrime = true;
            for (Integer number : listOfPrimes) {
                if (nr%number == 0) isPrime = false;
            }
            if (isPrime) listOfPrimes.add(nr);
            nr++;
        }
        return listOfPrimes;
    }
}
