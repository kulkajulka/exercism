import java.util.LinkedList;
import java.util.List;

public class PrimeCalculator {
    public Object nth(int i) {
        if (i < 1) throw new IllegalArgumentException();
        List<Integer> listOfPrimes = new LinkedList<>();
        listOfPrimes.add(2);
        int nr = 3;
        while (listOfPrimes.size() < i){
            boolean isPrime = true;
            for (Integer number : listOfPrimes) {
                if (nr%number == 0) isPrime = false;
            }
            if (isPrime) listOfPrimes.add(nr);
            nr++;
        }
        return listOfPrimes.get(i-1);
    }
}
